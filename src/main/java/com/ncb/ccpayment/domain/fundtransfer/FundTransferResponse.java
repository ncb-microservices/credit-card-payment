package com.ncb.ccpayment.domain.fundtransfer;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class FundTransferResponse {
	private String fromAccountNumber;
	private BigDecimal fromBalance;
	private BigDecimal toBalance;
	private String journalReferenceNumber;
}
