package com.ncb.ccpayment.domain.cardpayment;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class CardPaymentResponse {
	private String cardNumber;
	private BigDecimal availableBalance;
}
