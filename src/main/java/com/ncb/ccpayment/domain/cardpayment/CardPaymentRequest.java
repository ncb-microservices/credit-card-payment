package com.ncb.ccpayment.domain.cardpayment;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CardPaymentRequest {
	private String creditCardNumber;
	private BigDecimal amount;
	private String currency;
	private String journalNumber;
}
