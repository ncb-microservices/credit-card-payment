package com.ncb.ccpayment.domain;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class CreditCardPaymentRequest {
	String creditCardNumber;
	String fromAccountNumber;
	String toAccountNumber;
	BigDecimal amount;
	String currencyCode;
}
