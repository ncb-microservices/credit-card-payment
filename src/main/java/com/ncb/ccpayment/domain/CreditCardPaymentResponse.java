package com.ncb.ccpayment.domain;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CreditCardPaymentResponse {
	private String journalReferenceNumber;
	private BigDecimal creditCardAvailableBalance;
	private BigDecimal accountBalance;
}
