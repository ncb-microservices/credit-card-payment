package com.ncb.ccpayment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ImportResource;
import org.springframework.integration.config.EnableIntegration;

@SpringBootApplication
@EnableIntegration
@ImportResource("credit-card-payment.xml")
@EnableDiscoveryClient
public class CreditCardPaymentApplication {

	public static void main(String[] args) {
		SpringApplication.run(CreditCardPaymentApplication.class, args);
	}

}
