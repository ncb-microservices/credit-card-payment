package com.ncb.ccpayment.transformers;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.integration.annotation.Transformer;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import com.ncb.ccpayment.domain.CreditCardPaymentRequest;
import com.ncb.ccpayment.domain.CreditCardPaymentResponse;
import com.ncb.ccpayment.domain.cardpayment.CardPaymentRequest;
import com.ncb.ccpayment.domain.cardpayment.CardPaymentResponse;
import com.ncb.ccpayment.domain.fundtransfer.FundTransferRequest;
import com.ncb.ccpayment.domain.fundtransfer.FundTransferResponse;

@Configuration
public class TransformerConfiguration {

	@Bean(name = "httpMessageConverters")
	public List<HttpMessageConverter<?>> setupHttpMessageConverters() {
		return Arrays.asList(new MappingJackson2HttpMessageConverter());
	}

	/**
	 * Transforms the input message into the fund transfer request
	 * 
	 * @param message
	 * @return
	 */
	@Transformer(inputChannel = "creditCardPaymentRequest", outputChannel = "fundTransferRequest")
	public Message<FundTransferRequest> convert2FundTransferRequest(Message<CreditCardPaymentRequest> message) {
		CreditCardPaymentRequest creditCardPaymentRequest = message.getPayload();
		FundTransferRequest fundTransferRequest = new FundTransferRequest(
				creditCardPaymentRequest.getFromAccountNumber(), creditCardPaymentRequest.getToAccountNumber(),
				creditCardPaymentRequest.getAmount(), creditCardPaymentRequest.getCurrencyCode());

		return MessageBuilder.withPayload(fundTransferRequest)
				.setHeader("creditCardPaymentRequest", creditCardPaymentRequest).copyHeaders(message.getHeaders())
				.build();
	}

	/**
	 * Transforms the fund transfer response to card payment request
	 * 
	 * @param message
	 * @return
	 */
	@Transformer(inputChannel = "fundTransferResponse", outputChannel = "cardPaymentRequest")
	public Message<CardPaymentRequest> convert2CardPaymentRequest(Message<FundTransferResponse> message) {
		FundTransferResponse fundTransferResponse = message.getPayload();
		CreditCardPaymentRequest creditCardPaymentRequest = (CreditCardPaymentRequest) message.getHeaders()
				.get("creditCardPaymentRequest");

		CardPaymentRequest cardPaymentRequest = new CardPaymentRequest(creditCardPaymentRequest.getCreditCardNumber(),
				creditCardPaymentRequest.getAmount(), creditCardPaymentRequest.getCurrencyCode(),
				fundTransferResponse.getJournalReferenceNumber());

		return MessageBuilder.withPayload(cardPaymentRequest).copyHeaders(message.getHeaders())
				.setHeader("journalReferenceNumber", fundTransferResponse.getJournalReferenceNumber())
				.setHeader("accountBalance", fundTransferResponse.getToBalance()).build();
	}

	/**
	 * Transforms the card payment response to final response
	 * 
	 * @param message
	 * @return
	 */
	@Transformer(inputChannel = "cardPaymentResponse", outputChannel = "creditCardPaymentResponse")
	public Message<CreditCardPaymentResponse> convert2CreditCardPaymentResponse(Message<CardPaymentResponse> message) {
		CardPaymentResponse cardPaymentResponse = message.getPayload();

		CreditCardPaymentResponse response = new CreditCardPaymentResponse(
				message.getHeaders().get("journalReferenceNumber", String.class),
				cardPaymentResponse.getAvailableBalance(),
				message.getHeaders().get("accountBalance", BigDecimal.class));

		return MessageBuilder.withPayload(response).copyHeaders(message.getHeaders()).build();
	}
}
